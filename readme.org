#+title: Game Starter
* This is a simple launch point to make a game with c++, OpenGL, and GLUT.
* Installation
- Clone the repository.
- Run the [[./bin/init.sh][bin/init.sh]] script to create and setup the build directory.
- Run the [[./bin/buildAndRun.sh][bin/buildAndRun.sh]] script to compile the project and run the created executable.
The project is built with CMake, and will automatically link any files in the src directory. If you want to add subdirectories to the project, you will need to add them to the CMakeLists.txt file. (probably the one in the src directory)

* Assets
This project uses OpenGL and GLUT for graphics. Assets to be rendered are stored in the assets directory. See [[file:assets/readme.org][assets/readme.org]] for more details.
