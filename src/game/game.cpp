/*******************************************************************************\
 * File: game.cpp
 *
 * Description: Contains the implementations of the method bodies for the game
 * class.
\*******************************************************************************/
#include "game.h"
#include "../point/point.h"
#include "../uiDraw/uiDraw.h"
#include "../uiInteract/uiInteract.h"

using namespace std;

/*******************************************************************************\
 * GAME::Constructor :: Setup the initial state of the game
\*******************************************************************************/
Game::Game(Point tl, Point br)
    : topLeft(tl), bottomRight(br),
      gamePad(OpenGlObject("game-controller-arcade.json")),
      settings(Settings()){};

/*******************************************************************************\
 * GAME::HandleInput :: accept input from the user
\*******************************************************************************/
void Game::handleInput(const Interface &ui) {
  // Do things when user provides input. for example
  // if (ui.isLeft()) {
  //   // user has pressed the left arrow key handle that input.
  // }
}

/*******************************************************************************\
 * GAME::Draw :: draws everything for the game.
\*******************************************************************************/
void Game::draw(const Interface &ui) {
  drawText(Point(-195, -330), "Welcome to the Game Starter!");
  gamePad.draw();
}
