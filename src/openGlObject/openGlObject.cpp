#include "openGlObject.h"

#ifdef __APPLE__
#define GL_SILENCE_DEPRECATION
#include <GLUT/glut.h> // Second OpenGL library
#include <openGL/gl.h> // Main OpenGL library
#endif                 // __APPLE__

#ifdef __linux__
#include <GL/gl.h>   // Main OpenGL library
#include <GL/glut.h> // Second OpenGL library
#endif               // __linux__

#ifdef _WIN32
#include <GL/glut.h> // OpenGL library we copied
#include <stdio.h>
#include <stdlib.h>
#define _USE_MATH_DEFINES
#include <cmath>
#include <math.h>
#endif // _WIN32

#include <fstream>
#include <iostream>
using namespace std;

/*******************************************************************************\
 * OpenGlObject :: Read the json file and populate sections.
\******************************************************************************/
OpenGlObject::OpenGlObject(const char *filename, const Point &position,
                           int rotation, float scale)
    : Drawable(position, rotation, scale) {
  string file = "assets/opengl-objects/";
  file.append(filename);
  fstream f(file.c_str());
  if (f.fail()) {
    cerr << endl << "Failed to open " << filename << endl;
    cerr << "Is it in the assets/opengl-objects directory?" << endl;
  } else {
    json data = json::parse(f);
    vector<string> parts = (vector<string>)data["paths"];

    for (auto part : parts) {
      vector<json> pointsJson = (vector<json>)data[part]["points"];
      RGB fill =
          RGB((float)data[part]["fill"]["r"], (float)data[part]["fill"]["g"],
              (float)data[part]["fill"]["b"]);
      RGB stroke = RGB((float)data[part]["stroke"]["r"],
                       (float)data[part]["stroke"]["g"],
                       (float)data[part]["stroke"]["b"]);

      bool hasFill = fill.r != -1;
      bool hasStroke = stroke.r != -1;

      if (part.find("circle-") != string::npos) {
        const float radius = (float)data[part]["radius"];
        const PT circleCenter = PT((float)data[part]["center"]["x"],
                                   (float)data[part]["center"]["y"]);

        sections.push_back(new Section(Cir(circleCenter, radius), hasStroke,
                                       hasFill, stroke, fill));
      } else {
        vector<PT> points;
        for (auto point : pointsJson) {
          points.push_back({(float)point["x"], (float)point["y"]});
        }
        sections.push_back(
            new Section(points, hasStroke, hasFill, stroke, fill));
      }
    }
  }
}

/*******************************************************************************\
 * ~OpenGlObject :: Delete all sections.
\******************************************************************************/
OpenGlObject::~OpenGlObject() {
  for (auto section : sections) {
    delete section;
  }
}

/*******************************************************************************\
 * draw :: Draw all sections.
\*******************************************************************************/
void OpenGlObject::draw() {
  for (auto section : sections) {
    drawSection(*section);
  }
}

/*******************************************************************************\
 * Section::Draw :: Draw the section.
\*******************************************************************************/
void OpenGlObject::drawSection(const Section &section) {
  auto drawShape = [](const vector<PT> &points, const Point &center, RGB rgb,
                      bool fill, int rotation, float scale) {
    static int i = 0;
    glBegin(fill ? GL_POLYGON : GL_LINE_STRIP);
    glColor3ub(rgb.r, rgb.g, rgb.b);
    for (auto point : points) {
      Point pt(center.getX() + point.x * scale,
               center.getY() + point.y * scale);
      rotate(pt, center, rotation);
      glVertex2f(pt.getX(), pt.getY());
    }
    glEnd();
  };

  if (section.isCircle()) {
    Point circleCenter = Point(position);
    circleCenter.addX(section.circle.center.x * scale);
    circleCenter.addY(section.circle.center.y * scale);
    rotate(circleCenter, position, rotation + 180);
    if (section.hasFill) {
      drawCircle(circleCenter, section.circle.radius * scale, section.fill,
                 true);
    }
    if (section.hasStroke) {
      drawCircle(circleCenter, section.circle.radius * scale, section.stroke);
    }
  } else {
    if (section.hasFill) {
      drawShape(section.points, position, section.fill, true, rotation + 180,
                scale);
    }
    if (section.hasStroke) {
      drawShape(section.points, position, section.stroke, false, rotation + 180,
                scale);
    }
  }

  glColor3ub(255, 255, 255); // reset to white
}
