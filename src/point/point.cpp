/*******************************************************************************\
 * Source File:
 *    Point : The representation of a position on the screen
 * Summary:
 *    Everything we need to know about a location on the screen, including
 *    the location and the bounds.
\*******************************************************************************/

#include "point.h"
#include <cassert>

/*******************************************************************************\
 * POINT::Insertion :: Display coordinates on the screen
\*******************************************************************************/
std::ostream &operator<<(std::ostream &out, const Point &pt) {
  out << "(" << pt.getX() << ", " << pt.getY() << ")";
  return out;
}

/*******************************************************************************\
 * POINT::extraction :: Prompt for coordinates
\*******************************************************************************/
std::istream &operator>>(std::istream &in, Point &pt) {
  float x;
  float y;
  in >> x >> y;

  pt.setX(x);
  pt.setY(y);

  return in;
}
