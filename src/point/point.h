/*******************************************************************************\
 * Header File:
 *    Point : The representation of a position on the screen
 * Summary:
 *    Everything we need to know about a location on the screen, including
 *    the location and the bounds.
\*******************************************************************************/

#ifndef POINT_H
#define POINT_H

#include <iostream>

/*******************************************************************************\
 * POINT :: A single position.
\*******************************************************************************/
class Point {
public:
  // constructors
  Point() : x(0.0), y(0.0) {}
  Point(bool check) : x(0.0), y(0.0) {}
  Point(float x, float y) : x(x), y(y){};
  Point(const Point &p) : x(p.getX()), y(p.getY()){};

  // getters
  float getX() const { return x; }
  float getY() const { return y; }

  // setters
  void setX(float x) { this->x = x; };
  void setY(float y) { this->y = y; };
  void addX(float dx) { setX(getX() + dx); }
  void addY(float dy) { setY(getY() + dy); }

private:
  float x; // horizontal position
  float y; // vertical position
};

// stream I/O useful for debugging
std::ostream &operator<<(std::ostream &out, const Point &pt);
std::istream &operator>>(std::istream &in, Point &pt);

#endif // POINT_H
