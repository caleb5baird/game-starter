#include "../settings/settings.h"
#include <fstream>
#include <iostream>
#include <nlohmann/json.hpp>

using json = nlohmann::json;

/*****************************************************************************\
 * Settings::ResetAll :: Reset all settings to their default values
\*****************************************************************************/
void Settings::resetAll() { this->example = this->defaultExample; }

/*****************************************************************************\
 * Settings::LoadFromFile :: Load the settings from the settings.json file
\*****************************************************************************/
void Settings::loadFromFile() {
  std::fstream f("src/settings.json");
  if (f.fail()) {
    std::cerr << "Failed to open settings.json" << std::endl;
  } else {
    this->settings = json::parse(f);

    // Update the current values with the settings object
    this->example = (string)settings["example"];
  }
}

/*****************************************************************************\
 * Settings::WriteToFile :: Write the settings to the settings.json file
\*****************************************************************************/
void Settings::writeToFile() {
  // Update the settings object with the current values
  this->settings["example"] = this->example;

  // Write the settings object to the file
  std::ofstream f("src/settings.json");
  if (f.fail()) {
    std::cerr << "Failed to open settings.json" << std::endl;
  } else {
    f << this->settings.dump(2);
  }
}
