/*******************************************************************************\
 * File: settings.h
 *
 * Description: Handles all game settings. Settings are stored in the json file:
 *   "settings.json" so that they can have defined values between game instances
 *   but have defined defaults in the class.
 *
 * TODO::
 *   - Add an gui interface for changing settings.
 *   - Maybe introduce the concept of a "settings profile" so that difforent
 *     usrers can have different settings. This would require a concept of a
 *     user profile. Not sure yet if this is necessary.
\*******************************************************************************/
#ifndef SETTINGS_H_
#define SETTINGS_H_

#include <nlohmann/json.hpp>
#include <string>

using json = nlohmann::json;
using namespace std;

class Settings {
public:
  Settings() : defaultExample("Default") { this->loadFromFile(); }
  void save() { this->writeToFile(); }

  // setting variables
  string example;

  // reset to default
  void resetAll();
  string resetExample() { return this->example = this->defaultExample; }

private:
  void loadFromFile();
  void writeToFile();
  json settings;

  // setting defaults
  string defaultExample;
};

#endif // SETTINGS_H_
